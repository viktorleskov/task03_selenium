package seleniumhm;

import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import seleniumhm.driver.DriverClient;
import seleniumhm.pageobject.gmail.businessobject.GmailHomePageBO;
import seleniumhm.pageobject.gmail.businessobject.GmailLoginPageBO;
import seleniumhm.utils.ConfigReaders;

public class GmailTest {
    private static org.apache.logging.log4j.Logger LOG;
    private static WebDriver driver;

    @BeforeClass
    static void initAll() {
        LOG = LogManager.getLogger(GmailTest.class);
    }

    @BeforeMethod
    void init() {
        LOG.info("BeforeEach running.");
        driver = DriverClient.getDriver();
        driver.get(ConfigReaders.read("base_page_gmail"));
    }

    @Test
    void gmailSignInTest() {
        GmailLoginPageBO gmailLoginPageBO = new GmailLoginPageBO();
        gmailLoginPageBO
                .enterEmail("viktorius490")
                .enterPassword("supervl30111999");
        pause(5000);
    }

    @Test
    void gmailSendMessageTest(){
        GmailLoginPageBO gmailLoginPageBO = new GmailLoginPageBO();
        GmailHomePageBO gmailHomePageBO = new GmailHomePageBO();
        gmailLoginPageBO
                .enterEmail("viktorius490")
                .enterPassword("supervl30111999");
        pause(1500);
        gmailHomePageBO
                .sendMessage("viktorius490@gmail.com","hi from selenium","kuku")
                .clickSentMailButton();
    }

    @AfterMethod
    void tearDown() {
        LOG.info("After each running.");
       // DriverClient.quitDriver();
    }

    @AfterClass
    static void tearDownAll() {
        LOG.info("AfterAll test running.");
        LOG = null;
        driver = null;
    }

    private void pause(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
