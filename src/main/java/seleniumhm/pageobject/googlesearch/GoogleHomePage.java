package seleniumhm.pageobject.googlesearch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import seleniumhm.driver.DriverClient;
import seleniumhm.element.CustomDecorator;
import seleniumhm.element.usually.Button;
import seleniumhm.element.usually.TextInput;
import seleniumhm.utils.ElementWaiter;

public class GoogleHomePage {
    private static Logger LOG = LogManager.getLogger(GoogleHomePage.class);

    public GoogleHomePage() {
        PageFactory.initElements(new CustomDecorator(new DefaultElementLocatorFactory(DriverClient.getDriver())), this);
    }

    @FindBy(name = "q")
    private TextInput searchField;

    @FindBy(xpath = "(//a[@class='q qs'])[1]")
    private Button imageButton;

    public GoogleHomePage inputTextInSearchField(String text) {
        ElementWaiter.waitForVisibilityOfElement(searchField).sendKeys(text);
        LOG.info("text :" + text + " was entered");
        return this;
    }

    public GoogleHomePage submitSearchField() {
        ElementWaiter.waitForVisibilityOfElement(searchField).submit();
        LOG.info("search field was submitted");
        return this;
    }

    public GoogleHomePage clickImageButton() {
        ElementWaiter.waitForVisibilityOfElement(imageButton).click();
        LOG.info("image button clicked");
        return this;
    }
}
