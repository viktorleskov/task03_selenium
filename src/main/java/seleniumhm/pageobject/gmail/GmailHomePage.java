package seleniumhm.pageobject.gmail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import seleniumhm.driver.DriverClient;
import seleniumhm.element.CustomDecorator;
import seleniumhm.element.usually.Button;
import seleniumhm.element.usually.TextInput;
import seleniumhm.utils.ElementWaiter;

public class GmailHomePage {
    private static Logger LOG = LogManager.getLogger(GmailHomePage.class);

    public GmailHomePage() {
        PageFactory.initElements(new CustomDecorator(new DefaultElementLocatorFactory(DriverClient.getDriver())), this);
    }

    @FindBy(xpath = "//div[@jscontroller]//div[@role='button']")
    private Button composeButton;

    @FindBy(xpath = "//table[@role='group']//div[@role='button' and text()]")
    private Button sendButton;

    @FindBy(xpath = "(//input[@type='text' and @aria-label])[1]")
    private TextInput searchField;

    @FindBy(xpath = "//a[@target='_top' and contains(@href,'sent')]")
    private Button sentMailButton;

    @FindBy(xpath = "//textarea[@role='combobox']")
    private TextInput destinationEmailField;

    @FindBy(name = "subjectbox")
    private TextInput subjectBoxField;

    @FindBy(xpath = "//div[@role='textbox']")
    private TextInput messageTextField;

    public GmailHomePage clickComposeButton() {
        ElementWaiter.waitForVisibilityOfElement(composeButton).click();
        LOG.info("compose button was clicked");
        return this;
    }

    public GmailHomePage clickSendMessageButton() {
        ElementWaiter.waitForVisibilityOfElement(sendButton).click();
        LOG.info("send button was clicked");
        return this;
    }

    public GmailHomePage clickSentMailButton() {
        ElementWaiter.waitForVisibilityOfElement(sentMailButton).click();
        LOG.info("sentMail button was clicked");
        return this;
    }

    public GmailHomePage submitSearchField() {
        ElementWaiter.waitForVisibilityOfElement(searchField).submit();
        LOG.info(" search field was submitted");
        return this;
    }

    public GmailHomePage enterSeachField(String text) {
        ElementWaiter.waitForVisibilityOfElement(searchField).sendKeys(text);
        LOG.info("text " + text + " was entered into search field");
        return this;
    }

    public GmailHomePage enterDestinationEmail(String destinationEmail) {
        ElementWaiter.waitForVisibilityOfElement(destinationEmailField).sendKeys(destinationEmail + "\n");
        LOG.info(" email " + destinationEmail + " was entered");
        return this;
    }

    public GmailHomePage enterSubject(String subject) {
        ElementWaiter.waitForVisibilityOfElement(subjectBoxField).sendKeys(subject);
        LOG.info("subject " + subject + "was entered");
        return this;
    }

    public GmailHomePage enterMessageText(String message) {
        ElementWaiter.waitForVisibilityOfElement(messageTextField).sendKeys(message);
        LOG.info("message was entered");
        return this;
    }
}
