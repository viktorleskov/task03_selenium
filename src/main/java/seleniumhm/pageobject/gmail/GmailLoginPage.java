package seleniumhm.pageobject.gmail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import seleniumhm.driver.DriverClient;
import seleniumhm.element.CustomDecorator;
import seleniumhm.element.usually.Button;
import seleniumhm.element.usually.TextInput;
import seleniumhm.utils.ElementWaiter;

public class GmailLoginPage {
    private static Logger LOG = LogManager.getLogger(GmailLoginPage.class);

    public GmailLoginPage() {
        PageFactory.initElements(new CustomDecorator(new DefaultElementLocatorFactory(DriverClient.getDriver())), this);
    }

    @FindBy(xpath = "//input[@type='email']")
    private TextInput emailField;

    @FindBy(xpath = "//input[@type='password']")
    private TextInput passwordField;

    @FindBy(id = "identifierNext")
    private Button emailNextButton;

    @FindBy(id = "passwordNext")
    private Button passwordNextButton;

    public GmailLoginPage inputTextInEmailField(String text) {
        ElementWaiter.waitForVisibilityOfElement(emailField).sendKeys(text);
        LOG.info("text :" + text + " was entered");
        return this;
    }

    public GmailLoginPage inputTextInPasswordField(String text) {
        ElementWaiter.waitForVisibilityOfElement(passwordField).sendKeys(text);
        LOG.info("text :" + text + " was entered");
        return this;
    }

    public GmailLoginPage clickEmailNextButton() {
        ElementWaiter.waitForVisibilityOfElement(emailNextButton).click();
        LOG.info("email field was submitted");
        return this;
    }

    public GmailLoginPage clickPasswordNextButton() {
        ElementWaiter.waitForVisibilityOfElement(passwordNextButton).click();
        LOG.info("password field was submitted");
        return this;
    }

}
