package seleniumhm.pageobject.gmail.businessobject;

import seleniumhm.pageobject.gmail.GmailLoginPage;

public class GmailLoginPageBO {
    private GmailLoginPage gmailLoginPage;

    public GmailLoginPageBO() {
        gmailLoginPage = new GmailLoginPage();
    }

    public GmailLoginPageBO enterEmail(String email) {
        gmailLoginPage
                .inputTextInEmailField(email)
                .clickEmailNextButton();
        return this;
    }

    public GmailLoginPageBO enterPassword(String password) {
        gmailLoginPage
                .inputTextInPasswordField(password)
                .clickPasswordNextButton();
        return this;
    }
}
