package seleniumhm.pageobject.gmail.businessobject;

import seleniumhm.pageobject.gmail.GmailHomePage;

public class GmailHomePageBO {
    private GmailHomePage gmailHomePage;

    public GmailHomePageBO(){
        gmailHomePage = new GmailHomePage();
    }

    public GmailHomePageBO sendMessage(String email, String subject, String message){
        gmailHomePage
                .clickComposeButton()
                .enterDestinationEmail(email)
                .enterSubject(subject)
                .enterMessageText(message)
                .clickSendMessageButton();
        return this;
    }

    public GmailHomePageBO enterTextIntoSearchField(String text){
        gmailHomePage
                .enterSeachField(text)
                .submitSearchField();
        return this;
    }

    public GmailHomePageBO clickSentMailButton(){
        gmailHomePage
                .clickSentMailButton();
        return this;
    }

}
