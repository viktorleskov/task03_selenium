package seleniumhm.utils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import seleniumhm.driver.DriverClient;

public abstract class ElementWaiter {
    public static WebElement waitForVisibilityOfElement(WebElement element) {
        return (new WebDriverWait(DriverClient.getDriver(), Integer.parseInt(ConfigReaders.read("basic_explicit_timeout"))))
                .until(ExpectedConditions.visibilityOf(element));
    }
}
