package seleniumhm.driver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import seleniumhm.utils.ConfigReaders;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class DriverClient {
    private static Logger LOG = LogManager.getLogger(DriverClient.class);
    private static boolean useProfile = Boolean.parseBoolean(ConfigReaders.read("use_profile"));
    private static WebDriver driver;

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", ConfigReaders.read("chrome_driver"));
        return Optional.ofNullable(driver).isPresent() ?
                /*if present*/      driver :
                /*if NOT present*/  initializeDriver();
    }

    private static WebDriver initializeDriver() {
        driver = new ChromeDriver(prepareOptions());
        driver.manage().timeouts().
                implicitlyWait(Integer.parseInt(ConfigReaders.read("basic_implicit_timeout")), TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

    private static ChromeOptions prepareOptions(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("chrome.switches", "--disable-extensions");
        if(useProfile){
            options.addArguments("--user-data-dir=" + ConfigReaders.read("user_data_dir"),
                    "--profile-directory=" + ConfigReaders.read("profile_directory"));
        }
        return options;
    }

    public static void quitDriver() {
        Optional.ofNullable(driver).ifPresent(thenExecute -> {
            driver.quit();
            LOG.debug("WebDriver was closed!");
        });
    }
}
